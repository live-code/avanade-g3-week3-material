import { Component } from '@angular/core';

@Component({
  selector: 'fb-root',
  template: `
    <button routerLink="list-demo">demo list</button>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  title = 'avanade-g3-week3-material';
}
