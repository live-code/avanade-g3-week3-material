import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'list-demo', loadChildren: () => import('./features/list-demo/list-demo.module').then(m => m.ListDemoModule) },
  { path: '', redirectTo: 'list-demo', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
