import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {UserDetailsComponent} from "./components/user-details.component";
import {CdkDragDrop, moveItemInArray} from "@angular/cdk/drag-drop";

@Component({
  selector: 'fb-list-demo',
  styleUrls: ['./list-demo.component.css'],
  template: `
    <form #f="ngForm" (submit)="save(f)">
      <mat-form-field class="example-form-field" appearance="fill">
        <mat-label>Clearable input</mat-label>
        <input matInput type="text" [(ngModel)]="value" name="firstname">
        <button *ngIf="value" matSuffix mat-icon-button aria-label="Clear"
                (click)="value=''">
          <mat-icon>favorite</mat-icon>
        </button>
      </mat-form-field>

      <br>
      <mat-slider
        min="1" max="100" step="1" value="1"
        ngModel
        [thumbLabel]="true"
        name="age"
        required
      ></mat-slider>
      <button type="submit" [disabled]="f.invalid">save</button>
    </form>


    <mat-divider></mat-divider>


    <mat-list role="list" cdkDropList class="example-list" (cdkDropListDropped)="drop($event)">
      <mat-list-item  class="example-box"  role="listitem" *ngFor="let user of users" cdkDrag>

        <div mat-line>{{user.name}} - {{user.username}}</div>
        <button mat-icon-button>

          <button mat-icon-button [matMenuTriggerFor]="menu" aria-label="Example icon-button with a menu">
            <mat-icon>more_vert</mat-icon>
          </button>
          <mat-menu #menu="matMenu">
            <button mat-menu-item (click)="removeHandler(user)">
              <mat-icon>delete</mat-icon>
              <span>delete</span>
            </button>
            <button mat-menu-item (click)="openDetailsHandler(user)">
              <mat-icon>voicemail</mat-icon>
              <span>open details</span>
            </button>
          </mat-menu>
        </button>
      </mat-list-item>
    </mat-list>

    <pre>{{users | json}}</pre>
  `,
  styles: [
  ]
})
export class ListDemoComponent {
  value: string = '';
  users: any[] = [];

  constructor(private http: HttpClient, public dialog: MatDialog) {
    http.get<any[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => this.users = res)
  }
  save(form: NgForm) {
    console.log(form.value)
  }

  removeHandler(user: any) {
    this.http.delete<any[]>('https://jsonplaceholder.typicode.com/users/' + user.id)
      .subscribe(() => {
        this.users = this.users.filter(u => u.id !== user.id)
      })
  }

  openDetailsHandler(user: any) {
    const dialogRef = this.dialog.open(UserDetailsComponent, {
      data: JSON.parse(JSON.stringify(user))
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result`, result);
      this.http.patch<any>('https://jsonplaceholder.typicode.com/users/' + user.id, result)
        .subscribe(res => {
          this.users = this.users.map(u => {
            return u.id === user.id ? res : u;
          })
        })

    });
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.users, event.previousIndex, event.currentIndex);
  }
}
