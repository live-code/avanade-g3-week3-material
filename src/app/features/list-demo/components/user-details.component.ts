import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'fb-user-details',
  template: `
    <h2 mat-dialog-title>Edit {{user.name}}</h2>

    <form #f="ngForm" (submit)="save(f.value)">
      <mat-dialog-content class="mat-typography">
        <mat-form-field class="example-form-field" appearance="fill">
          <mat-label>Name</mat-label>
          <input matInput type="text" [ngModel]="user.name" name="name" required>
        </mat-form-field>

        <mat-form-field class="example-form-field" appearance="outline">
          <mat-label>Username</mat-label>
          <input matInput type="text" [ngModel]="user.username" name="username" required>
        </mat-form-field>


      </mat-dialog-content>

      <mat-dialog-actions align="end">
        <button mat-button mat-dialog-close>Cancel</button>
        <button type="submit" mat-button  cdkFocusInitial [disabled]="f.invalid">SAVE</button>
      </mat-dialog-actions>
    </form>

  `,
  styles: [
  ]
})
export class UserDetailsComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public user: any,
    private matDialogRef: MatDialogRef<any>
  ) {
  }

  ngOnInit(): void {
  }

  save(formData: any) {
    console.log(formData)
    this.matDialogRef.close(formData)
  }
}
