import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListDemoRoutingModule } from './list-demo-routing.module';
import { ListDemoComponent } from './list-demo.component';
import {MatSliderModule} from "@angular/material/slider";
import {FormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";
import {MatListModule} from "@angular/material/list";
import {MatDividerModule} from "@angular/material/divider";
import {MatButtonModule} from "@angular/material/button";
import {MatMenuModule} from "@angular/material/menu";
import {MatDialogModule} from "@angular/material/dialog";
import { UserDetailsComponent } from './components/user-details.component';
import {DragDropModule} from "@angular/cdk/drag-drop";


@NgModule({
  declarations: [
    ListDemoComponent,
    UserDetailsComponent
  ],
  imports: [
    CommonModule,
    ListDemoRoutingModule,
    MatSliderModule,
    FormsModule,
    MatInputModule,
    MatIconModule,
    MatListModule,
    MatDividerModule,
    MatButtonModule,
    MatMenuModule,
    MatDialogModule,
    DragDropModule

  ]
})
export class ListDemoModule { }
